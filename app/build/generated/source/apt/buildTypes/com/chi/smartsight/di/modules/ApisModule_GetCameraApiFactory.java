package com.chi.smartsight.di.modules;

import com.chi.smartsight.api.dao.camera.camera.CameraApi;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class ApisModule_GetCameraApiFactory implements Factory<CameraApi> {
  private final ApisModule module;

  public ApisModule_GetCameraApiFactory(ApisModule module) {
    assert module != null;
    this.module = module;
  }

  @Override
  public CameraApi get() {
    return Preconditions.checkNotNull(
        module.getCameraApi(), "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<CameraApi> create(ApisModule module) {
    return new ApisModule_GetCameraApiFactory(module);
  }

  /** Proxies {@link ApisModule#getCameraApi()}. */
  public static CameraApi proxyGetCameraApi(ApisModule instance) {
    return instance.getCameraApi();
  }
}
