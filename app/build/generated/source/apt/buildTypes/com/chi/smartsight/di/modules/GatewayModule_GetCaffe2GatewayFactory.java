package com.chi.smartsight.di.modules;

import com.chi.smartsight.gateway.processor.Caffe2Gateway;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class GatewayModule_GetCaffe2GatewayFactory implements Factory<Caffe2Gateway> {
  private final GatewayModule module;

  public GatewayModule_GetCaffe2GatewayFactory(GatewayModule module) {
    assert module != null;
    this.module = module;
  }

  @Override
  public Caffe2Gateway get() {
    return Preconditions.checkNotNull(
        module.getCaffe2Gateway(), "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<Caffe2Gateway> create(GatewayModule module) {
    return new GatewayModule_GetCaffe2GatewayFactory(module);
  }

  /** Proxies {@link GatewayModule#getCaffe2Gateway()}. */
  public static Caffe2Gateway proxyGetCaffe2Gateway(GatewayModule instance) {
    return instance.getCaffe2Gateway();
  }
}
