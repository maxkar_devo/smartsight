package com.chi.smartsight.di.modules;

import com.chi.smartsight.gateway.camera.Camera2Gateway;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class GatewayModule_GetCamera2GatewayFactory implements Factory<Camera2Gateway> {
  private final GatewayModule module;

  public GatewayModule_GetCamera2GatewayFactory(GatewayModule module) {
    assert module != null;
    this.module = module;
  }

  @Override
  public Camera2Gateway get() {
    return Preconditions.checkNotNull(
        module.getCamera2Gateway(), "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<Camera2Gateway> create(GatewayModule module) {
    return new GatewayModule_GetCamera2GatewayFactory(module);
  }

  /** Proxies {@link GatewayModule#getCamera2Gateway()}. */
  public static Camera2Gateway proxyGetCamera2Gateway(GatewayModule instance) {
    return instance.getCamera2Gateway();
  }
}
