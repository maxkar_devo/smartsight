package com.chi.smartsight.di.modules;

import com.chi.smartsight.presenter.camera.CameraActivityPresenter;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class PresenterModule_GetCameraActivityInteractorFactory
    implements Factory<CameraActivityPresenter> {
  private final PresenterModule module;

  public PresenterModule_GetCameraActivityInteractorFactory(PresenterModule module) {
    assert module != null;
    this.module = module;
  }

  @Override
  public CameraActivityPresenter get() {
    return Preconditions.checkNotNull(
        module.getCameraActivityInteractor(),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<CameraActivityPresenter> create(PresenterModule module) {
    return new PresenterModule_GetCameraActivityInteractorFactory(module);
  }

  /** Proxies {@link PresenterModule#getCameraActivityInteractor()}. */
  public static CameraActivityPresenter proxyGetCameraActivityInteractor(PresenterModule instance) {
    return instance.getCameraActivityInteractor();
  }
}
