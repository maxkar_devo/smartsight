package com.chi.smartsight.interactor.camera;

import com.chi.smartsight.gateway.camera.Camera2Gateway;
import com.chi.smartsight.gateway.camera.CameraGateway;
import com.chi.smartsight.gateway.processor.Caffe2Gateway;
import dagger.MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class CameraViewInteractor_MembersInjector
    implements MembersInjector<CameraViewInteractor> {
  private final Provider<Caffe2Gateway> caffe2GatewayProvider;

  private final Provider<CameraGateway> cameraGatewayProvider;

  private final Provider<Camera2Gateway> camera2GatewayProvider;

  public CameraViewInteractor_MembersInjector(
      Provider<Caffe2Gateway> caffe2GatewayProvider,
      Provider<CameraGateway> cameraGatewayProvider,
      Provider<Camera2Gateway> camera2GatewayProvider) {
    assert caffe2GatewayProvider != null;
    this.caffe2GatewayProvider = caffe2GatewayProvider;
    assert cameraGatewayProvider != null;
    this.cameraGatewayProvider = cameraGatewayProvider;
    assert camera2GatewayProvider != null;
    this.camera2GatewayProvider = camera2GatewayProvider;
  }

  public static MembersInjector<CameraViewInteractor> create(
      Provider<Caffe2Gateway> caffe2GatewayProvider,
      Provider<CameraGateway> cameraGatewayProvider,
      Provider<Camera2Gateway> camera2GatewayProvider) {
    return new CameraViewInteractor_MembersInjector(
        caffe2GatewayProvider, cameraGatewayProvider, camera2GatewayProvider);
  }

  @Override
  public void injectMembers(CameraViewInteractor instance) {
    if (instance == null) {
      throw new NullPointerException("Cannot inject members into a null reference");
    }
    instance.caffe2Gateway = caffe2GatewayProvider.get();
    instance.cameraGateway = cameraGatewayProvider.get();
    instance.camera2Gateway = camera2GatewayProvider.get();
  }

  public static void injectCaffe2Gateway(
      CameraViewInteractor instance, Provider<Caffe2Gateway> caffe2GatewayProvider) {
    instance.caffe2Gateway = caffe2GatewayProvider.get();
  }

  public static void injectCameraGateway(
      CameraViewInteractor instance, Provider<CameraGateway> cameraGatewayProvider) {
    instance.cameraGateway = cameraGatewayProvider.get();
  }

  public static void injectCamera2Gateway(
      CameraViewInteractor instance, Provider<Camera2Gateway> camera2GatewayProvider) {
    instance.camera2Gateway = camera2GatewayProvider.get();
  }
}
