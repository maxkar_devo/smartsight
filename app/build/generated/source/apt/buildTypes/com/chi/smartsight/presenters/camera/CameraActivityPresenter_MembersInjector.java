package com.chi.smartsight.presenter.camera;

import com.chi.smartsight.interactor.camera.CameraViewInteractor;
import dagger.MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class CameraActivityPresenter_MembersInjector
    implements MembersInjector<CameraActivityPresenter> {
  private final Provider<CameraViewInteractor> cameraViewInteractorProvider;

  public CameraActivityPresenter_MembersInjector(
      Provider<CameraViewInteractor> cameraViewInteractorProvider) {
    assert cameraViewInteractorProvider != null;
    this.cameraViewInteractorProvider = cameraViewInteractorProvider;
  }

  public static MembersInjector<CameraActivityPresenter> create(
      Provider<CameraViewInteractor> cameraViewInteractorProvider) {
    return new CameraActivityPresenter_MembersInjector(cameraViewInteractorProvider);
  }

  @Override
  public void injectMembers(CameraActivityPresenter instance) {
    if (instance == null) {
      throw new NullPointerException("Cannot inject members into a null reference");
    }
    instance.cameraViewInteractor = cameraViewInteractorProvider.get();
  }

  public static void injectCameraViewInteractor(
      CameraActivityPresenter instance,
      Provider<CameraViewInteractor> cameraViewInteractorProvider) {
    instance.cameraViewInteractor = cameraViewInteractorProvider.get();
  }
}
