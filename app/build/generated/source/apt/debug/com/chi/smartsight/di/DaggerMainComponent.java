package com.chi.smartsight.di;

import com.chi.smartsight.api.dao.caffe.Caffe2Api;
import com.chi.smartsight.api.dao.camera.camera.CameraApi;
import com.chi.smartsight.api.dao.camera.camera2.Camera2Api;
import com.chi.smartsight.di.modules.ApisModule;
import com.chi.smartsight.di.modules.ApisModule_GetCaffe2ApiFactory;
import com.chi.smartsight.di.modules.ApisModule_GetCamera2ApiFactory;
import com.chi.smartsight.di.modules.ApisModule_GetCameraApiFactory;
import com.chi.smartsight.di.modules.ApplicationModule;
import com.chi.smartsight.di.modules.DatabaseModule;
import com.chi.smartsight.di.modules.GatewayModule;
import com.chi.smartsight.di.modules.GatewayModule_GetCaffe2GatewayFactory;
import com.chi.smartsight.di.modules.GatewayModule_GetCamera2GatewayFactory;
import com.chi.smartsight.di.modules.GatewayModule_GetCameraGatewayFactory;
import com.chi.smartsight.di.modules.InteractorModule;
import com.chi.smartsight.di.modules.InteractorModule_GetCameraViewInteractorFactory;
import com.chi.smartsight.di.modules.PresenterModule;
import com.chi.smartsight.di.modules.PresenterModule_GetCameraActivityInteractorFactory;
import com.chi.smartsight.gateway.camera.Camera2Gateway;
import com.chi.smartsight.gateway.camera.Camera2Gateway_MembersInjector;
import com.chi.smartsight.gateway.camera.CameraGateway;
import com.chi.smartsight.gateway.camera.CameraGateway_MembersInjector;
import com.chi.smartsight.gateway.processor.Caffe2Gateway;
import com.chi.smartsight.gateway.processor.Caffe2Gateway_MembersInjector;
import com.chi.smartsight.interactor.camera.CameraViewInteractor;
import com.chi.smartsight.interactor.camera.CameraViewInteractor_MembersInjector;
import com.chi.smartsight.presenter.camera.CameraActivityPresenter;
import com.chi.smartsight.presenter.camera.CameraActivityPresenter_MembersInjector;
import com.chi.smartsight.ui.activity.camera.CameraActivity;
import com.chi.smartsight.ui.activity.camera.CameraActivity_MembersInjector;
import dagger.MembersInjector;
import dagger.internal.DoubleCheck;
import dagger.internal.Preconditions;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class DaggerMainComponent implements MainComponent {
  private Provider<CameraApi> getCameraApiProvider;

  private MembersInjector<CameraGateway> cameraGatewayMembersInjector;

  private Provider<Camera2Api> getCamera2ApiProvider;

  private MembersInjector<Camera2Gateway> camera2GatewayMembersInjector;

  private Provider<Caffe2Api> getCaffe2ApiProvider;

  private MembersInjector<Caffe2Gateway> caffe2GatewayMembersInjector;

  private Provider<Caffe2Gateway> getCaffe2GatewayProvider;

  private Provider<CameraGateway> getCameraGatewayProvider;

  private Provider<Camera2Gateway> getCamera2GatewayProvider;

  private MembersInjector<CameraViewInteractor> cameraViewInteractorMembersInjector;

  private Provider<CameraViewInteractor> getCameraViewInteractorProvider;

  private MembersInjector<CameraActivityPresenter> cameraActivityPresenterMembersInjector;

  private Provider<CameraActivityPresenter> getCameraActivityInteractorProvider;

  private MembersInjector<CameraActivity> cameraActivityMembersInjector;

  private DaggerMainComponent(Builder builder) {
    assert builder != null;
    initialize(builder);
  }

  public static Builder builder() {
    return new Builder();
  }

  @SuppressWarnings("unchecked")
  private void initialize(final Builder builder) {

    this.getCameraApiProvider =
        DoubleCheck.provider(ApisModule_GetCameraApiFactory.create(builder.apisModule));

    this.cameraGatewayMembersInjector = CameraGateway_MembersInjector.create(getCameraApiProvider);

    this.getCamera2ApiProvider =
        DoubleCheck.provider(ApisModule_GetCamera2ApiFactory.create(builder.apisModule));

    this.camera2GatewayMembersInjector =
        Camera2Gateway_MembersInjector.create(getCamera2ApiProvider);

    this.getCaffe2ApiProvider =
        DoubleCheck.provider(ApisModule_GetCaffe2ApiFactory.create(builder.apisModule));

    this.caffe2GatewayMembersInjector = Caffe2Gateway_MembersInjector.create(getCaffe2ApiProvider);

    this.getCaffe2GatewayProvider =
        DoubleCheck.provider(GatewayModule_GetCaffe2GatewayFactory.create(builder.gatewayModule));

    this.getCameraGatewayProvider =
        DoubleCheck.provider(GatewayModule_GetCameraGatewayFactory.create(builder.gatewayModule));

    this.getCamera2GatewayProvider =
        DoubleCheck.provider(GatewayModule_GetCamera2GatewayFactory.create(builder.gatewayModule));

    this.cameraViewInteractorMembersInjector =
        CameraViewInteractor_MembersInjector.create(
            getCaffe2GatewayProvider, getCameraGatewayProvider, getCamera2GatewayProvider);

    this.getCameraViewInteractorProvider =
        DoubleCheck.provider(
            InteractorModule_GetCameraViewInteractorFactory.create(builder.interactorModule));

    this.cameraActivityPresenterMembersInjector =
        CameraActivityPresenter_MembersInjector.create(getCameraViewInteractorProvider);

    this.getCameraActivityInteractorProvider =
        DoubleCheck.provider(
            PresenterModule_GetCameraActivityInteractorFactory.create(builder.presenterModule));

    this.cameraActivityMembersInjector =
        CameraActivity_MembersInjector.create(getCameraActivityInteractorProvider);
  }

  @Override
  public void inject(CameraGateway cameraGateway) {
    cameraGatewayMembersInjector.injectMembers(cameraGateway);
  }

  @Override
  public void inject(Camera2Gateway camera2Gateway) {
    camera2GatewayMembersInjector.injectMembers(camera2Gateway);
  }

  @Override
  public void inject(Caffe2Gateway caffe2Gateway) {
    caffe2GatewayMembersInjector.injectMembers(caffe2Gateway);
  }

  @Override
  public void inject(CameraViewInteractor cameraViewInteractor) {
    cameraViewInteractorMembersInjector.injectMembers(cameraViewInteractor);
  }

  @Override
  public void inject(CameraActivityPresenter cameraActivityPresenter) {
    cameraActivityPresenterMembersInjector.injectMembers(cameraActivityPresenter);
  }

  @Override
  public void inject(CameraActivity cameraActivity) {
    cameraActivityMembersInjector.injectMembers(cameraActivity);
  }

  public static final class Builder {
    private ApisModule apisModule;

    private GatewayModule gatewayModule;

    private InteractorModule interactorModule;

    private PresenterModule presenterModule;

    private Builder() {}

    public MainComponent build() {
      if (apisModule == null) {
        throw new IllegalStateException(ApisModule.class.getCanonicalName() + " must be set");
      }
      if (gatewayModule == null) {
        throw new IllegalStateException(GatewayModule.class.getCanonicalName() + " must be set");
      }
      if (interactorModule == null) {
        throw new IllegalStateException(InteractorModule.class.getCanonicalName() + " must be set");
      }
      if (presenterModule == null) {
        throw new IllegalStateException(PresenterModule.class.getCanonicalName() + " must be set");
      }
      return new DaggerMainComponent(this);
    }

    /**
     * @deprecated This module is declared, but an instance is not used in the component. This
     *     method is a no-op. For more, see https://google.github.io/dagger/unused-modules.
     */
    @Deprecated
    public Builder baseModule(BaseModule baseModule) {
      Preconditions.checkNotNull(baseModule);
      return this;
    }

    /**
     * @deprecated This module is declared, but an instance is not used in the component. This
     *     method is a no-op. For more, see https://google.github.io/dagger/unused-modules.
     */
    @Deprecated
    public Builder applicationModule(ApplicationModule applicationModule) {
      Preconditions.checkNotNull(applicationModule);
      return this;
    }

    /**
     * @deprecated This module is declared, but an instance is not used in the component. This
     *     method is a no-op. For more, see https://google.github.io/dagger/unused-modules.
     */
    @Deprecated
    public Builder databaseModule(DatabaseModule databaseModule) {
      Preconditions.checkNotNull(databaseModule);
      return this;
    }

    public Builder apisModule(ApisModule apisModule) {
      this.apisModule = Preconditions.checkNotNull(apisModule);
      return this;
    }

    public Builder presenterModule(PresenterModule presenterModule) {
      this.presenterModule = Preconditions.checkNotNull(presenterModule);
      return this;
    }

    public Builder interactorModule(InteractorModule interactorModule) {
      this.interactorModule = Preconditions.checkNotNull(interactorModule);
      return this;
    }

    public Builder gatewayModule(GatewayModule gatewayModule) {
      this.gatewayModule = Preconditions.checkNotNull(gatewayModule);
      return this;
    }
  }
}
