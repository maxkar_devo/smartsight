package com.chi.smartsight.di.modules;

import com.chi.smartsight.api.dao.camera.camera2.Camera2Api;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class ApisModule_GetCamera2ApiFactory implements Factory<Camera2Api> {
  private final ApisModule module;

  public ApisModule_GetCamera2ApiFactory(ApisModule module) {
    assert module != null;
    this.module = module;
  }

  @Override
  public Camera2Api get() {
    return Preconditions.checkNotNull(
        module.getCamera2Api(), "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<Camera2Api> create(ApisModule module) {
    return new ApisModule_GetCamera2ApiFactory(module);
  }

  /** Proxies {@link ApisModule#getCamera2Api()}. */
  public static Camera2Api proxyGetCamera2Api(ApisModule instance) {
    return instance.getCamera2Api();
  }
}
