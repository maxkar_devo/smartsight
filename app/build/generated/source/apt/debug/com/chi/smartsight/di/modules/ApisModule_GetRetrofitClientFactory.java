package com.chi.smartsight.di.modules;

import com.chi.smartsight.api.clients.rest.RetrofitClient;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class ApisModule_GetRetrofitClientFactory implements Factory<RetrofitClient> {
  private final ApisModule module;

  public ApisModule_GetRetrofitClientFactory(ApisModule module) {
    assert module != null;
    this.module = module;
  }

  @Override
  public RetrofitClient get() {
    return Preconditions.checkNotNull(
        module.getRetrofitClient(), "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<RetrofitClient> create(ApisModule module) {
    return new ApisModule_GetRetrofitClientFactory(module);
  }

  /** Proxies {@link ApisModule#getRetrofitClient()}. */
  public static RetrofitClient proxyGetRetrofitClient(ApisModule instance) {
    return instance.getRetrofitClient();
  }
}
