package com.chi.smartsight.gateway.camera;

import com.chi.smartsight.api.dao.camera.camera2.Camera2Api;
import dagger.MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class Camera2Gateway_MembersInjector implements MembersInjector<Camera2Gateway> {
  private final Provider<Camera2Api> camera2ApiProvider;

  public Camera2Gateway_MembersInjector(Provider<Camera2Api> camera2ApiProvider) {
    assert camera2ApiProvider != null;
    this.camera2ApiProvider = camera2ApiProvider;
  }

  public static MembersInjector<Camera2Gateway> create(Provider<Camera2Api> camera2ApiProvider) {
    return new Camera2Gateway_MembersInjector(camera2ApiProvider);
  }

  @Override
  public void injectMembers(Camera2Gateway instance) {
    if (instance == null) {
      throw new NullPointerException("Cannot inject members into a null reference");
    }
    instance.camera2Api = camera2ApiProvider.get();
  }

  public static void injectCamera2Api(
      Camera2Gateway instance, Provider<Camera2Api> camera2ApiProvider) {
    instance.camera2Api = camera2ApiProvider.get();
  }
}
