package com.chi.smartsight.ui.activity.camera;

import com.chi.smartsight.presenter.camera.CameraActivityPresenter;
import dagger.MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class CameraActivity_MembersInjector implements MembersInjector<CameraActivity> {
  private final Provider<CameraActivityPresenter> activityPresenterProvider;

  public CameraActivity_MembersInjector(
      Provider<CameraActivityPresenter> activityPresenterProvider) {
    assert activityPresenterProvider != null;
    this.activityPresenterProvider = activityPresenterProvider;
  }

  public static MembersInjector<CameraActivity> create(
      Provider<CameraActivityPresenter> activityPresenterProvider) {
    return new CameraActivity_MembersInjector(activityPresenterProvider);
  }

  @Override
  public void injectMembers(CameraActivity instance) {
    if (instance == null) {
      throw new NullPointerException("Cannot inject members into a null reference");
    }
    instance.activityPresenter = activityPresenterProvider.get();
  }

  public static void injectActivityPresenter(
      CameraActivity instance, Provider<CameraActivityPresenter> activityPresenterProvider) {
    instance.activityPresenter = activityPresenterProvider.get();
  }
}
