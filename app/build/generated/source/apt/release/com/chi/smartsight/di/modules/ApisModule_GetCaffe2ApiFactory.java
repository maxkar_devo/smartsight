package com.chi.smartsight.di.modules;

import com.chi.smartsight.api.dao.caffe.Caffe2Api;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class ApisModule_GetCaffe2ApiFactory implements Factory<Caffe2Api> {
  private final ApisModule module;

  public ApisModule_GetCaffe2ApiFactory(ApisModule module) {
    assert module != null;
    this.module = module;
  }

  @Override
  public Caffe2Api get() {
    return Preconditions.checkNotNull(
        module.getCaffe2Api(), "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<Caffe2Api> create(ApisModule module) {
    return new ApisModule_GetCaffe2ApiFactory(module);
  }

  /** Proxies {@link ApisModule#getCaffe2Api()}. */
  public static Caffe2Api proxyGetCaffe2Api(ApisModule instance) {
    return instance.getCaffe2Api();
  }
}
