package com.chi.smartsight.di.modules;

import com.chi.smartsight.gateway.camera.CameraGateway;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class GatewayModule_GetCameraGatewayFactory implements Factory<CameraGateway> {
  private final GatewayModule module;

  public GatewayModule_GetCameraGatewayFactory(GatewayModule module) {
    assert module != null;
    this.module = module;
  }

  @Override
  public CameraGateway get() {
    return Preconditions.checkNotNull(
        module.getCameraGateway(), "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<CameraGateway> create(GatewayModule module) {
    return new GatewayModule_GetCameraGatewayFactory(module);
  }

  /** Proxies {@link GatewayModule#getCameraGateway()}. */
  public static CameraGateway proxyGetCameraGateway(GatewayModule instance) {
    return instance.getCameraGateway();
  }
}
