package com.chi.smartsight.di.modules;

import com.chi.smartsight.interactor.camera.CameraViewInteractor;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.annotation.Generated;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class InteractorModule_GetCameraViewInteractorFactory
    implements Factory<CameraViewInteractor> {
  private final InteractorModule module;

  public InteractorModule_GetCameraViewInteractorFactory(InteractorModule module) {
    assert module != null;
    this.module = module;
  }

  @Override
  public CameraViewInteractor get() {
    return Preconditions.checkNotNull(
        module.getCameraViewInteractor(),
        "Cannot return null from a non-@Nullable @Provides method");
  }

  public static Factory<CameraViewInteractor> create(InteractorModule module) {
    return new InteractorModule_GetCameraViewInteractorFactory(module);
  }

  /** Proxies {@link InteractorModule#getCameraViewInteractor()}. */
  public static CameraViewInteractor proxyGetCameraViewInteractor(InteractorModule instance) {
    return instance.getCameraViewInteractor();
  }
}
