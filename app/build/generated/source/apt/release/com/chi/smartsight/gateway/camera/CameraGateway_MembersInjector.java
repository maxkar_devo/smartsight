package com.chi.smartsight.gateway.camera;

import com.chi.smartsight.api.dao.camera.camera.CameraApi;
import dagger.MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class CameraGateway_MembersInjector implements MembersInjector<CameraGateway> {
  private final Provider<CameraApi> cameraApiProvider;

  public CameraGateway_MembersInjector(Provider<CameraApi> cameraApiProvider) {
    assert cameraApiProvider != null;
    this.cameraApiProvider = cameraApiProvider;
  }

  public static MembersInjector<CameraGateway> create(Provider<CameraApi> cameraApiProvider) {
    return new CameraGateway_MembersInjector(cameraApiProvider);
  }

  @Override
  public void injectMembers(CameraGateway instance) {
    if (instance == null) {
      throw new NullPointerException("Cannot inject members into a null reference");
    }
    instance.cameraApi = cameraApiProvider.get();
  }

  public static void injectCameraApi(
      CameraGateway instance, Provider<CameraApi> cameraApiProvider) {
    instance.cameraApi = cameraApiProvider.get();
  }
}
