package com.chi.smartsight.gateway.processor;

import com.chi.smartsight.api.dao.caffe.Caffe2Api;
import dagger.MembersInjector;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
  value = "dagger.internal.codegen.ComponentProcessor",
  comments = "https://google.github.io/dagger"
)
public final class Caffe2Gateway_MembersInjector implements MembersInjector<Caffe2Gateway> {
  private final Provider<Caffe2Api> caffe2ApiProvider;

  public Caffe2Gateway_MembersInjector(Provider<Caffe2Api> caffe2ApiProvider) {
    assert caffe2ApiProvider != null;
    this.caffe2ApiProvider = caffe2ApiProvider;
  }

  public static MembersInjector<Caffe2Gateway> create(Provider<Caffe2Api> caffe2ApiProvider) {
    return new Caffe2Gateway_MembersInjector(caffe2ApiProvider);
  }

  @Override
  public void injectMembers(Caffe2Gateway instance) {
    if (instance == null) {
      throw new NullPointerException("Cannot inject members into a null reference");
    }
    instance.caffe2Api = caffe2ApiProvider.get();
  }

  public static void injectCaffe2Api(
      Caffe2Gateway instance, Provider<Caffe2Api> caffe2ApiProvider) {
    instance.caffe2Api = caffe2ApiProvider.get();
  }
}
