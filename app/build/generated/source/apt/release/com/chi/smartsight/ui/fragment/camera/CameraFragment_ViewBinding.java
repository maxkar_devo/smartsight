// Generated code from Butter Knife. Do not modify!
package com.chi.smartsight.ui.fragment.camera;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.chi.smartsight.R;
import com.github.mikephil.charting.charts.HorizontalBarChart;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CameraFragment_ViewBinding<T extends CameraFragment> implements Unbinder {
  protected T target;

  private View view2131624090;

  private View view2131624088;

  @UiThread
  public CameraFragment_ViewBinding(final T target, View source) {
    this.target = target;

    View view;
    target.frameLayout = Utils.findRequiredViewAsType(source, R.id.surfaceViewHolder, "field 'frameLayout'", FrameLayout.class);
    view = Utils.findRequiredView(source, R.id.iv_smart_eye, "field 'ivSmartEye' and method 'smartEyeBtnClick'");
    target.ivSmartEye = Utils.castView(view, R.id.iv_smart_eye, "field 'ivSmartEye'", ImageView.class);
    view2131624090 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.smartEyeBtnClick();
      }
    });
    view = Utils.findRequiredView(source, R.id.predictor_message_one, "field 'tvPrediction' and method 'tvPredictorClick'");
    target.tvPrediction = Utils.castView(view, R.id.predictor_message_one, "field 'tvPrediction'", TextView.class);
    view2131624088 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.tvPredictorClick();
      }
    });
    target.barChart = Utils.findRequiredViewAsType(source, R.id.c_bar_chart, "field 'barChart'", HorizontalBarChart.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.frameLayout = null;
    target.ivSmartEye = null;
    target.tvPrediction = null;
    target.barChart = null;

    view2131624090.setOnClickListener(null);
    view2131624090 = null;
    view2131624088.setOnClickListener(null);
    view2131624088 = null;

    this.target = null;
  }
}
