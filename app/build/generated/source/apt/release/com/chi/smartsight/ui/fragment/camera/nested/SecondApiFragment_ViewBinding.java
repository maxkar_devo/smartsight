// Generated code from Butter Knife. Do not modify!
package com.chi.smartsight.ui.fragment.camera.nested;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.TextureView;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.chi.smartsight.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SecondApiFragment_ViewBinding<T extends SecondApiFragment> implements Unbinder {
  protected T target;

  @UiThread
  public SecondApiFragment_ViewBinding(T target, View source) {
    this.target = target;

    target.textureView = Utils.findRequiredViewAsType(source, R.id.textureView, "field 'textureView'", TextureView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    T target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");

    target.textureView = null;

    this.target = null;
  }
}
