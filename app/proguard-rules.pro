# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /Users/bwasti/Library/Android/sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile

# Retrofit
-dontwarn retrofit2.**
-keep class retrofit2.* { ; }
-dontwarn org.codehaus.mojo.**
-keepattributes Signature
-keepattributes Exceptions
-keepattributes Annotation
-keepattributes RuntimeVisibleAnnotations
-keepattributes RuntimeInvisibleAnnotations
-keepattributes RuntimeVisibleParameterAnnotations
-keepattributes RuntimeInvisibleParameterAnnotations
-keepattributes EnclosingMethod
-keepclasseswithmembers class * {
    @retrofit2.http.* ;
}
-keepclasseswithmembers interface * {
    @retrofit2.* ;
}
# Okhttp
-dontwarn okio.**
-dontwarn okhttp3.**
-keep class okhttp3.* { ;}
-dontwarn com.squareup.okhttp.**
# GSON
-keep class com.google.gson.* { ; }
-keep class com.google.inject.* { ; }
-keep class org.apache.http.* { ; }
