package com.chi.smartsight.api.clients;


import com.chi.smartsight.api.dao.API;


public interface IBaseClient {

    API getClientApi();

}
