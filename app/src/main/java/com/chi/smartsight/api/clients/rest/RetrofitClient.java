package com.chi.smartsight.api.clients.rest;

import com.chi.smartsight.api.dao.rest.RestApi;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    private RestApi client;

    public RetrofitClient() {
        this.client = buildRestClient();
    }

    @SuppressWarnings("unused")
    public RestApi getClientApi() {
        return client;
    }

    private RestApi buildRestClient(){
        return /*setupClient(BuildConfig.contactBaseUrl).create(RestApi.class)*/null;
    }

    private Retrofit setupClient(String baseUrl) {

        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(getParser()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(getSubClient())
                .build();
    }

    private OkHttpClient getSubClient() {
        return new OkHttpClient.Builder()
                .build();
    }

    private Gson getParser() {
        return new GsonBuilder()
                .create();
    }

}
