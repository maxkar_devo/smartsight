package com.chi.smartsight.api.dao;

import android.content.Context;


public abstract class BaseAPI implements API{

    protected Context context;

    public BaseAPI(Context context) {
        this.context = context;
    }
}
