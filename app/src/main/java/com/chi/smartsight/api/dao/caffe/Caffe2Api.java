package com.chi.smartsight.api.dao.caffe;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.res.AssetManager;
import android.media.Image;
import android.util.Log;

import com.chi.smartsight.api.dao.BaseAPI;
import com.chi.smartsight.listener.camera.ICamDataUser;
import com.chi.smartsight.model.dto.Capture;

import java.nio.ByteBuffer;


@SuppressWarnings("WeakerAccess")
public class Caffe2Api extends BaseAPI {

    private static final String TAG = Caffe2Api.class.getCanonicalName();

    private Activity activity;

    @SuppressWarnings("unused")
    private ICamDataUser iCamDataUser;

    public Caffe2Api(Context context) {
        super(context);
    }

    public native boolean initCaffe2(AssetManager mgr);

    private native String classificationFromCaffe2
            (int h, int w, byte[] Y, byte[] U, byte[] V,
             int rowStride, int pixelStride, boolean r_hwc);

    public void loadLibrary() {
        System.loadLibrary("native-lib");
    }

    public void injectSource(ICamDataUser iCamDataUser) {

        this.iCamDataUser = iCamDataUser;
        this.activity = ((Fragment) iCamDataUser).getActivity();
    }

    public boolean initApi() {
        return initCaffe2(this.activity.getResources().getAssets());
    }

    public String processYUVData(Image image) {

        try {
            int w = image.getWidth();
            int h = image.getHeight();

            ByteBuffer Ybuffer = image.getPlanes()[0].getBuffer();
            ByteBuffer Ubuffer = image.getPlanes()[1].getBuffer();
            ByteBuffer Vbuffer = image.getPlanes()[2].getBuffer();

            int rowStride = image.getPlanes()[1].getRowStride();
            int pixelStride = image.getPlanes()[1].getPixelStride();

            byte[] Y = new byte[Ybuffer.capacity()];
            byte[] U = new byte[Ubuffer.capacity()];
            byte[] V = new byte[Vbuffer.capacity()];

            Ybuffer.get(Y);
            Ubuffer.get(U);
            Vbuffer.get(V);

            String predictedClass = classificationFromCaffe2(h, w, Y, U, V,
                    rowStride, pixelStride, false);

            Log.d(TAG, "processYUVData() called with: image = [" + predictedClass + "]");

            return predictedClass != null && !predictedClass.isEmpty() ?
                    predictedClass : "Could`t process image";

        } finally {
            if (image != null) {
                image.close();
            }
        }
    }

    public String processRGBData(Capture capture) {

        String predictedClass = classificationFromCaffe2(capture.getWidth(), capture.getHeight()
                , capture.getYuvBytes(), capture.getYuvBytes()
                , capture.getYuvBytes(), capture.getRowStride()
                , capture.getPixelStride(), false);

        Log.d(TAG, "processRGBData() called with: image = [" + predictedClass + "]");

        return predictedClass != null && !predictedClass.isEmpty() ?
                predictedClass : "Could`t process image";

    }

}
