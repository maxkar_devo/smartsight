package com.chi.smartsight.api.dao.camera;


import com.chi.smartsight.api.dao.API;

public interface ICameraAPI<T,N> extends API {

    void openCamera();

    void injectProcessor(T t);

    void injectSource(N n);

    void releaseCamera();

}
