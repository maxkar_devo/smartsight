package com.chi.smartsight.api.dao.camera.camera;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.util.Log;
import android.view.Display;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;

import com.chi.smartsight.api.dao.BaseAPI;
import com.chi.smartsight.api.dao.caffe.Caffe2Api;
import com.chi.smartsight.api.dao.camera.ICameraAPI;
import com.chi.smartsight.listener.camera.ICamDataUser;
import com.chi.smartsight.model.dto.Capture;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;

import static android.content.Context.WINDOW_SERVICE;


@SuppressWarnings("deprecation")
public class CameraApi extends BaseAPI implements ICameraAPI<Caffe2Api, ICamDataUser> {

    private static final String TAG = CameraApi.class.getCanonicalName();

    private Camera mCamera;
    private SurfaceHolder mHolder;
    private SurfaceView surfaceView;

    private Caffe2Api caffe2Api;
    private ICamDataUser iCamDataUser;
    private Context context;
    private boolean isPreviewRunning;

    public CameraApi(Context context) {
        super(context);

        this.context = context;
    }

    public void injectSource(ICamDataUser iCamDataUser) {
        this.iCamDataUser = iCamDataUser;
        this.surfaceView = (SurfaceView) this.iCamDataUser.provideCamView();
    }

    @Override
    public void openCamera() {

        mCamera = Camera.open(0);
        List<Camera.Size> sizes  = mCamera.getParameters().getSupportedPictureSizes();
        mCamera.getParameters().setPreviewSize(sizes.get(0).width, sizes.get(0).height);
        mCamera.setPreviewCallback(previewCallback);

        initComponents();
    }

    @Override
    public void injectProcessor(Caffe2Api caffe2Api) {
        this.caffe2Api = caffe2Api;
    }

    @Override
    public void releaseCamera() {

        if (mCamera != null) {
            mHolder.removeCallback(surfaceCallback);
            mCamera.setPreviewCallback(null);
            mCamera.stopPreview();
            mCamera.release();
            mCamera = null;
        }
    }

    private void initComponents() {

        mHolder = surfaceView.getHolder();
        mHolder.setFormat(/*YUV_420_888*/ImageFormat.NV21);
        mHolder.addCallback(surfaceCallback);
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    public void refreshCamera() {

        if (mHolder.getSurface() == null) {
            Log.d(TAG, "refreshCamera: Surface equals null!!!");
            return;
        }

        try {
            mCamera.stopPreview();
        } catch (Exception e) {
            Log.d(TAG, "refreshCamera:" + e.toString());
        }

        try {
            mCamera.setPreviewDisplay(mHolder);
            mCamera.startPreview();
            isPreviewRunning = true;
        } catch (Exception e) {
            Log.d(TAG, "Error starting camera preview: " + e.getMessage());
        }
    }

    @SuppressWarnings("SuspiciousNameCombination")
    private void rotatePreview(int width, int height) {

        Camera.Parameters parameters = mCamera.getParameters();
        Display display = ((WindowManager) context.getSystemService(WINDOW_SERVICE)).getDefaultDisplay();

        if (display.getRotation() == Surface.ROTATION_0) {
            parameters.setPreviewSize(height, width);
            mCamera.setDisplayOrientation(90);
        }

        if (display.getRotation() == Surface.ROTATION_90) {
            parameters.setPreviewSize(width, height);
        }

        if (display.getRotation() == Surface.ROTATION_180) {
            parameters.setPreviewSize(height, width);
        }

        if (display.getRotation() == Surface.ROTATION_270) {
            parameters.setPreviewSize(width, height);
            mCamera.setDisplayOrientation(180);
        }

        mCamera.setParameters(parameters);
    }

    private Camera.PreviewCallback previewCallback = new Camera.PreviewCallback() {
        @Override
        public void onPreviewFrame(byte[] data, Camera camera) {

            //convert YUV to RGB
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            YuvImage yuvImage=new YuvImage(data, ImageFormat.NV21, mCamera.getParameters().getPreviewSize().width, mCamera.getParameters().getPreviewSize().height, null);
            yuvImage.compressToJpeg(new Rect(0, 0, mCamera.getParameters().getPreviewSize().width, mCamera.getParameters().getPreviewSize().height), 50, out);
            byte[] imageBytes = out.toByteArray();

            //scale bitmap for processor
            Bitmap image = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
            Bitmap resized = Bitmap.createScaledBitmap(image, 170, 170, true);

            //obtain target buffer
            int bytes = resized.getByteCount();
            ByteBuffer buffer = ByteBuffer.allocate(bytes);
            resized.copyPixelsToBuffer(buffer);
            byte[] scaledData = buffer.array();

            //process data
            String result = caffe2Api.processRGBData(new Capture(224, 224, scaledData, 224, 3));
            iCamDataUser.getResult(result);
        }
    };

    private SurfaceHolder.Callback surfaceCallback = new SurfaceHolder.Callback() {
        @Override
        public void surfaceCreated(SurfaceHolder holder) {

            try {
                openCamera();
                mCamera.setPreviewDisplay(holder);
                mCamera.startPreview();
            } catch (IOException e) {
                Log.d(TAG, "Error setting camera preview: " + e.getMessage());
            }
        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

            if (isPreviewRunning) {
                mCamera.stopPreview();
            }

            rotatePreview(width, height);
            refreshCamera();
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            releaseCamera();
        }
    };
}
