package com.chi.smartsight.application;

import android.app.Application;

import com.chi.smartsight.di.DaggerMainComponent;
import com.chi.smartsight.di.MainComponent;
import com.chi.smartsight.di.modules.ApisModule;
import com.chi.smartsight.di.modules.ApplicationModule;
import com.chi.smartsight.di.modules.DatabaseModule;
import com.chi.smartsight.di.modules.GatewayModule;
import com.chi.smartsight.di.modules.InteractorModule;
import com.chi.smartsight.di.modules.PresenterModule;


public class App extends Application {

    private static MainComponent mainComponent;

    public static MainComponent getMainComponent() {
        return mainComponent;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onCreate() {
        super.onCreate();

        mainComponent = DaggerMainComponent
                .builder()
                .applicationModule(new ApplicationModule(this))
                .apisModule(new ApisModule(this))
                .databaseModule(new DatabaseModule(this))
                .presenterModule(new PresenterModule(this))
                .interactorModule(new InteractorModule(this))
                .gatewayModule(new GatewayModule(this))
                .build();
    }
}
