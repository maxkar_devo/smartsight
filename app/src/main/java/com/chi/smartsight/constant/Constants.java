package com.chi.smartsight.constant;


import android.content.UriMatcher;
import android.net.Uri;
import android.provider.BaseColumns;

import java.util.HashMap;

public class Constants {

    public static class Database {

        @SuppressWarnings("WeakerAccess")
        public static class DataProviders {

            public static final String DATABASE = "smartsight_db";

            public static final int DB_VERSION = 1;

            public static final String TABLE_NAME = "frame";

            public static final String AUTHORITY = "com.chi.smartsight.AppContentProvider";

            public static final String SCHEME = "content://";

            public static final String FIRST_PATH = "frames";

            @SuppressWarnings("unused")
            public static final String CONTENT_URL = SCHEME + AUTHORITY;

            public static final Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY);

            public static final int FLAG_ONE = 1;

            @SuppressWarnings("unused")
            public static final int FLAG_ONE_OBTAIN = 2;

            public static final UriMatcher uriMatcher;

            public static HashMap<String, String> CONSTANTS_FRAMES_LIST_PROJECTION;

            static {

                uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
                uriMatcher.addURI(AUTHORITY, FIRST_PATH + "/", FLAG_ONE);

                CONSTANTS_FRAMES_LIST_PROJECTION = new HashMap<>();
                CONSTANTS_FRAMES_LIST_PROJECTION.put(FramesProviderData.COLUMN_ID, FramesProviderData.COLUMN_ID);

            }

        }

        public static class Query{

            public static final String FRAME_TABLE_CREATE =
                    "create table " + DataProviders.TABLE_NAME + "(" +
                            FramesProviderData.COLUMN_ID + " integer primary key autoincrement" +
                            ");";

            public static  final String FRAME_TABLE_DROP = "DROP TABLE IF EXISTS " + DataProviders.TABLE_NAME;

        }

        @SuppressWarnings("WeakerAccess")
        public static class FramesProviderData implements BaseColumns {

            public static final String COLUMN_ID = "id";

        }

    }
}
