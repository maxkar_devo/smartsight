package com.chi.smartsight.db.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.chi.smartsight.constant.Constants;


public class AppDataSourceHelper extends SQLiteOpenHelper {

    public AppDataSourceHelper(Context context, AppDatabaseErrorHandler errorHandler) {
        super(context, Constants.Database.DataProviders.DATABASE, null, Constants.Database.DataProviders.DB_VERSION, errorHandler);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(Constants.Database.Query.FRAME_TABLE_CREATE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        Log.w(AppDataSourceHelper.class.getCanonicalName(), "Upgrading database from version " + oldVersion + " to " + newVersion + ", which will destroy all old data");
        db.execSQL(Constants.Database.Query.FRAME_TABLE_DROP);

        onCreate(db);
    }
}
