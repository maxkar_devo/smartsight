package com.chi.smartsight.db.helper;

import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;


public class AppDatabaseErrorHandler implements DatabaseErrorHandler {

    @Override
    public void onCorruption(SQLiteDatabase dbObj) {
        if (dbObj.isOpen()) {
            dbObj.close();
        }
    }
}
