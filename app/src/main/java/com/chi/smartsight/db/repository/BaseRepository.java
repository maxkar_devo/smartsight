package com.chi.smartsight.db.repository;

import android.content.Context;
import android.database.Cursor;

import com.chi.smartsight.db.helper.AppDataSourceHelper;

@SuppressWarnings("unused")
public abstract class BaseRepository<T, N> implements IRepository<T, N> {

    protected Context context;

    @SuppressWarnings("WeakerAccess")
    protected AppDataSourceHelper appDataSourceHelper;

    protected BaseRepository(Context context) {
        this.context = context;
    }

    protected void closeDatabase() {
        if (appDataSourceHelper != null) {
            appDataSourceHelper.close();
        }
    }

    protected void closeCursor(Cursor cursor) {
        if (cursor != null) {
            cursor.close();
        }
    }
}
