package com.chi.smartsight.db.repository;

import java.util.List;

@SuppressWarnings({"WeakerAccess", "unused"})
public interface IRepository<T, N> {

    List<T> obtainAll();

    T getByIdentifier(N identifier);

    void deleteByIdentifier(N identifier);

    void save(T object);

    void update(T object);

    void deleteAll();

}
