package com.chi.smartsight.db.repository.camera;


import android.content.Context;

import com.chi.smartsight.db.repository.BaseRepository;
import com.chi.smartsight.model.entities.Frame;

import java.util.List;


@SuppressWarnings("unused")
public class FrameRepository extends BaseRepository<Frame, Long> {

    public FrameRepository(Context context) {
        super(context);
    }

    @Override
    public List<Frame> obtainAll() {
        return null;
    }

    @Override
    public Frame getByIdentifier(Long identifier) {
        return null;
    }

    @Override
    public void deleteByIdentifier(Long identifier) {

    }

    @Override
    public void save(Frame object) {

    }

    @Override
    public void update(Frame object) {

    }

    @Override
    public void deleteAll() {

    }
}
