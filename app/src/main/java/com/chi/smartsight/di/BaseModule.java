package com.chi.smartsight.di;


import android.content.Context;

import dagger.Module;

@Module
public class BaseModule {

    protected Context context;

    public BaseModule(Context context) {
        this.context = context;
    }
}
