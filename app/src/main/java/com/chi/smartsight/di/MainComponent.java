package com.chi.smartsight.di;

import com.chi.smartsight.di.modules.ApisModule;
import com.chi.smartsight.di.modules.ApplicationModule;
import com.chi.smartsight.di.modules.DatabaseModule;
import com.chi.smartsight.di.modules.GatewayModule;
import com.chi.smartsight.di.modules.InteractorModule;
import com.chi.smartsight.di.modules.PresenterModule;
import com.chi.smartsight.gateway.camera.Camera2Gateway;
import com.chi.smartsight.gateway.camera.CameraGateway;
import com.chi.smartsight.gateway.processor.Caffe2Gateway;
import com.chi.smartsight.interactor.camera.CameraViewInteractor;
import com.chi.smartsight.presenter.camera.CameraActivityPresenter;
import com.chi.smartsight.ui.activity.camera.CameraActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {BaseModule.class, ApplicationModule.class, DatabaseModule.class,
        ApisModule.class, PresenterModule.class, InteractorModule.class, GatewayModule.class})
public interface MainComponent {

    void inject(CameraGateway cameraGateway);

    void inject(Camera2Gateway camera2Gateway);

    void inject(Caffe2Gateway caffe2Gateway);

    void inject(CameraViewInteractor cameraViewInteractor);

    void inject(CameraActivityPresenter cameraActivityPresenter);

    void inject(CameraActivity cameraActivity);

}
