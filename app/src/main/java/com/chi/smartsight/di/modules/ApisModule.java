package com.chi.smartsight.di.modules;

import android.content.Context;

import com.chi.smartsight.api.clients.rest.RetrofitClient;
import com.chi.smartsight.api.dao.caffe.Caffe2Api;
import com.chi.smartsight.api.dao.camera.camera.CameraApi;
import com.chi.smartsight.api.dao.camera.camera2.Camera2Api;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ApisModule {

    private Context context;

    public ApisModule(Context context) {
        this.context = context;
    }

    @Provides
    @Singleton
    Caffe2Api getCaffe2Api() {
        return new Caffe2Api(context);
    }

    @Provides
    @Singleton
    Camera2Api getCamera2Api() {
        return new Camera2Api(context);
    }

    @Provides
    @Singleton
    CameraApi getCameraApi() {
        return new CameraApi(context);
    }

    @Provides
    @Singleton
    RetrofitClient getRetrofitClient() {
        return new RetrofitClient();
    }

}
