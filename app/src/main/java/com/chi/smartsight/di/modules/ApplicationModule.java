package com.chi.smartsight.di.modules;

import android.app.Application;
import android.content.Context;

import com.chi.smartsight.di.BaseModule;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule extends BaseModule{

    private Application application;

    public ApplicationModule(Application application) {
        super(application);

        this.application = application;
    }

    @Singleton
    @Provides
    Context getContext(){
        return context;
    }

}
