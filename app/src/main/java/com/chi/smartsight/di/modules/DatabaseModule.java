package com.chi.smartsight.di.modules;

import android.content.Context;


import com.chi.smartsight.di.BaseModule;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class DatabaseModule extends BaseModule {

    public DatabaseModule(Context context) {
        super(context);
    }
}
