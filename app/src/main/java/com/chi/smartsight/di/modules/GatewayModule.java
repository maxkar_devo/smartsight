package com.chi.smartsight.di.modules;

import android.content.Context;

import com.chi.smartsight.di.BaseModule;
import com.chi.smartsight.gateway.camera.Camera2Gateway;
import com.chi.smartsight.gateway.camera.CameraGateway;
import com.chi.smartsight.gateway.processor.Caffe2Gateway;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class GatewayModule extends BaseModule {

    public GatewayModule(Context context) {
        super(context);
    }

    @Singleton
    @Provides
    CameraGateway getCameraGateway(){
        return new CameraGateway(context);
    }

    @Singleton
    @Provides
    Camera2Gateway getCamera2Gateway(){
        return new Camera2Gateway(context);
    }

    @Singleton
    @Provides
    Caffe2Gateway getCaffe2Gateway(){
        return new Caffe2Gateway(context);
    }

}
