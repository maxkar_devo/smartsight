package com.chi.smartsight.di.modules;

import android.content.Context;

import com.chi.smartsight.di.BaseModule;
import com.chi.smartsight.interactor.camera.CameraViewInteractor;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;


@Module
public class InteractorModule extends BaseModule{

    public InteractorModule(Context context) {
        super(context);
    }

    @Singleton
    @Provides
    CameraViewInteractor getCameraViewInteractor(){
        return new CameraViewInteractor(context);
    }

}
