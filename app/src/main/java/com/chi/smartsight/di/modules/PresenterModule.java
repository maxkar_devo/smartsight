package com.chi.smartsight.di.modules;


import android.content.Context;

import com.chi.smartsight.di.BaseModule;
import com.chi.smartsight.presenter.camera.CameraActivityPresenter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class PresenterModule extends BaseModule{

    public PresenterModule(Context context) {
        super(context);
    }

    @Singleton
    @Provides
    CameraActivityPresenter getCameraActivityInteractor(){
        return new CameraActivityPresenter(context);
    }
}
