package com.chi.smartsight.gateway;


import android.content.Context;


public abstract class BaseGateway{

    private static final String TAG = BaseGateway.class.getSimpleName();

    private Context context;

    public BaseGateway(Context context) {
        this.context = context;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }
}
