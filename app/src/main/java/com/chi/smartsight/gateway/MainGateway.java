package com.chi.smartsight.gateway;


import android.content.Context;


/**
 * Will resolve connections between specific interactor & required gateways
 */
public class MainGateway extends BaseGateway{

    private static final String TAG = MainGateway.class.getSimpleName();

    public MainGateway(Context context) {
        super(context);
    }
}
