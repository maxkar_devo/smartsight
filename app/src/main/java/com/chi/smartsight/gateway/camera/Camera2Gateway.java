package com.chi.smartsight.gateway.camera;

import android.content.Context;

import com.chi.smartsight.api.dao.camera.camera2.Camera2Api;
import com.chi.smartsight.application.App;
import com.chi.smartsight.gateway.BaseGateway;

import javax.inject.Inject;


@SuppressWarnings("WeakerAccess")
public class Camera2Gateway extends BaseGateway implements ICameraGateway<Camera2Api>{

    private static final String TAG = Camera2Gateway.class.getSimpleName();

    @Inject
    Camera2Api camera2Api;

    public Camera2Gateway(Context context) {
        super(context);

        App.getMainComponent().inject(Camera2Gateway.this);
    }

    @Override
    public Camera2Api getCameraApi() {
        return camera2Api;
    }
}
