package com.chi.smartsight.gateway.camera;


import android.content.Context;

import com.chi.smartsight.api.dao.camera.camera.CameraApi;
import com.chi.smartsight.application.App;
import com.chi.smartsight.gateway.BaseGateway;

import javax.inject.Inject;

@SuppressWarnings("WeakerAccess")
public class CameraGateway extends BaseGateway implements ICameraGateway<CameraApi>{

    private static final String TAG = CameraGateway.class.getSimpleName();

    @Inject
    CameraApi cameraApi;

    public CameraGateway(Context context) {
        super(context);

        App.getMainComponent().inject(CameraGateway.this);
    }

    @Override
    public CameraApi getCameraApi() {
        return cameraApi;
    }
}
