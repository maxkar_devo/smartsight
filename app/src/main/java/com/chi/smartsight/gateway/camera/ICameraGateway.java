package com.chi.smartsight.gateway.camera;


import com.chi.smartsight.gateway.IGateway;

@SuppressWarnings("WeakerAccess")
public interface ICameraGateway<T> extends IGateway<T> {

    T getCameraApi();
}
