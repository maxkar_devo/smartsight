package com.chi.smartsight.gateway.processor;


import android.content.Context;

import com.chi.smartsight.api.dao.caffe.Caffe2Api;
import com.chi.smartsight.application.App;
import com.chi.smartsight.gateway.BaseGateway;
import com.chi.smartsight.interactor.camera.CameraViewInteractor;

import javax.inject.Inject;

@SuppressWarnings("WeakerAccess")
public class Caffe2Gateway extends BaseGateway implements IProcessorGateway<Caffe2Api> {

    private static final String TAG = Caffe2Gateway.class.getSimpleName();

    @Inject
    Caffe2Api caffe2Api;

    public Caffe2Gateway(Context context) {
        super(context);

        App.getMainComponent().inject(Caffe2Gateway.this);
    }

    @Override
    public Caffe2Api getProcessor() {
        return caffe2Api;
    }
}
