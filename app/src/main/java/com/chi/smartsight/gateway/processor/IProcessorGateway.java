package com.chi.smartsight.gateway.processor;


import com.chi.smartsight.gateway.IGateway;

public interface IProcessorGateway<T> extends IGateway<T>{

    T getProcessor();

}
