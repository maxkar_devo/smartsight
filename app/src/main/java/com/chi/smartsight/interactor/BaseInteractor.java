package com.chi.smartsight.interactor;


import android.content.Context;


public abstract class BaseInteractor<T, N, E> implements IInteractor<T, N, E> {

    private static final String TAG = BaseInteractor.class.getSimpleName();

    private Context context;

    public BaseInteractor(Context context) {
        this.context = context;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

}
