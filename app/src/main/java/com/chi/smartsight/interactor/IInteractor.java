package com.chi.smartsight.interactor;


@SuppressWarnings("WeakerAccess")
public interface IInteractor<T,N,E> {

    void extractDao();

    void startInteraction();

    T interact(N n, E e);

    void stopInteraction();
}
