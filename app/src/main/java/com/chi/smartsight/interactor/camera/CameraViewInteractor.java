package com.chi.smartsight.interactor.camera;


import android.content.Context;
import android.util.Log;
import android.view.TextureView;

import com.chi.smartsight.api.dao.caffe.Caffe2Api;
import com.chi.smartsight.api.dao.camera.camera.CameraApi;
import com.chi.smartsight.api.dao.camera.camera2.Camera2Api;
import com.chi.smartsight.application.App;
import com.chi.smartsight.gateway.camera.Camera2Gateway;
import com.chi.smartsight.gateway.camera.CameraGateway;
import com.chi.smartsight.gateway.processor.Caffe2Gateway;
import com.chi.smartsight.interactor.BaseInteractor;
import com.chi.smartsight.listener.camera.ICamDataUser;
import com.chi.smartsight.tool.CommonTool;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.annotations.NonNull;

public class CameraViewInteractor extends BaseInteractor<Void, ICamDataUser, Void> {

    private static final String TAG = CameraViewInteractor.class.getSimpleName();

    @SuppressWarnings("WeakerAccess")
    @Inject
    Caffe2Gateway caffe2Gateway;

    @SuppressWarnings("WeakerAccess")
    @Inject
    CameraGateway cameraGateway;

    @SuppressWarnings("WeakerAccess")
    @Inject
    Camera2Gateway camera2Gateway;

    private Caffe2Api caffe2Api;

    private Camera2Api camera2Api;

    private CameraApi cameraApi;

    private boolean isLibraryLoaded = false;

    public CameraViewInteractor(Context context) {
        super(context);

        App.getMainComponent().inject(CameraViewInteractor.this);
    }

    @Override
    public void extractDao() {

        caffe2Api = caffe2Gateway.getProcessor();
        cameraApi = cameraGateway.getCameraApi();
        camera2Api = camera2Gateway.getCameraApi();
    }

    @Override
    public void startInteraction() {
        Log.d(TAG, "startInteraction: CameraViewInteractor work started!!!");
    }

    @Override
    public Void interact(ICamDataUser iCamDataUser, Void aVoid) {

        activatePrediction(iCamDataUser);
        return null;
    }

    @Override
    public void stopInteraction() {
        unsubscribeApis();
    }

    private void activatePrediction(ICamDataUser iCamDataUser) {

        if (!isLibraryLoaded) {
            initCaffe2Api(iCamDataUser);
        }

        if (CommonTool.checkOSVersion()) {

            initCamera2Api(iCamDataUser);
        } else {
            initCameraApi(iCamDataUser);
        }
    }

    private void initCameraApi(ICamDataUser iCamDataUser) {

        cameraApi.injectSource(iCamDataUser);
        cameraApi.injectProcessor(caffe2Api);
        cameraApi.openCamera();
        cameraApi.refreshCamera();
    }

    private void initCamera2Api(ICamDataUser iCamDataUser) {

        camera2Api.injectProcessor(caffe2Api);
        camera2Api.injectSource(iCamDataUser);
        camera2Api.startBackgroundThread();
        if (((TextureView) iCamDataUser.provideCamView()).isAvailable()) {
            camera2Api.setupCamera((iCamDataUser.provideCamView()).getWidth(),
                    (iCamDataUser.provideCamView()).getHeight());
            camera2Api.openCamera();
        } else {
            ((TextureView) iCamDataUser.provideCamView()).setSurfaceTextureListener(camera2Api.getSurfaceTextureListener());
        }
    }

    private void initCaffe2Api(ICamDataUser iCamDataUser) {

        try {
            caffe2Api.loadLibrary();
            caffe2Api.injectSource(iCamDataUser);
            isLibraryLoaded = caffe2Api.initApi();
        } catch (Exception e) {
            Log.d(TAG, "initCaffe2Api: Could not init Caffe2 network");
        }
    }

    private void unsubscribeApis() {

        if (caffe2Api != null) {
            camera2Api.releaseCamera();
        }

        if (cameraApi != null) {
            cameraApi.releaseCamera();
        }
    }

    @SuppressWarnings("unused")
    private Observable observe(final ICamDataUser iCamDataUser) {

        return Observable.create(new ObservableOnSubscribe<Void>() {
            @Override
            public void subscribe(@NonNull final ObservableEmitter<Void> e) throws Exception {
                Log.d(TAG, "subscribe: ");
            }
        });
    }
}
