package com.chi.smartsight.listener;


public interface IDataProvider<T, N, E> extends IListener<T, N, E> {

    T dataProviding(N n, E e);
}
