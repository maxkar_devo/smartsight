package com.chi.smartsight.listener;


public interface IDataUser<T, N, E> extends IListener<T, N, E>{

    T dataObtaining(N n, E e);
}
