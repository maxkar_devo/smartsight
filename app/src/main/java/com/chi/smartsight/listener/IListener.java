package com.chi.smartsight.listener;

public interface IListener<T, N, E> {

    void stopListening();

    void clearData();

}
