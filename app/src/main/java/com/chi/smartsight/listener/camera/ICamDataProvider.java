package com.chi.smartsight.listener.camera;

import android.media.Image;

import com.chi.smartsight.listener.IDataProvider;


public interface ICamDataProvider extends IDataProvider<Image, Image, ICamDataUser> {

    @Override
    Image dataProviding(Image image, ICamDataUser iCamDataUser);
}
