package com.chi.smartsight.listener.camera;

import android.media.Image;
import android.view.View;

import com.chi.smartsight.listener.IDataUser;

public interface ICamDataUser extends IDataUser<Image, Image, String> {

    @Override
    Image dataObtaining(Image image, String s);

    View provideCamView();

    void getResult(String s);

}
