package com.chi.smartsight.model.dto;


public class Capture {

    private int width;

    private int height;

    private byte[] yuvBytes;

    private int rowStride;

    private int pixelStride;

    public Capture() {
    }

    public Capture(int width, int height, byte[] yuvBytes, int rowStride, int pixelStride) {

        this.width = width;
        this.height = height;
        this.yuvBytes = yuvBytes;
        this.rowStride = rowStride;
        this.pixelStride = pixelStride;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public byte[] getYuvBytes() {
        return yuvBytes;
    }

    public void setYuvBytes(byte[] yuvBytes) {
        this.yuvBytes = yuvBytes;
    }

    public int getRowStride() {
        return rowStride;
    }

    public void setRowStride(int rowStride) {
        this.rowStride = rowStride;
    }

    public int getPixelStride() {
        return pixelStride;
    }

    public void setPixelStride(int pixelStride) {
        this.pixelStride = pixelStride;
    }


}
