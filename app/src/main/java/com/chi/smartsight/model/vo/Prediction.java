package com.chi.smartsight.model.vo;


import com.chi.smartsight.tool.StringTool;

import java.util.ArrayList;

public class Prediction {

    private ArrayList<String> predictionCrumbs;

    public Prediction(ArrayList<String> predictionCrumbs) {
        this.predictionCrumbs = predictionCrumbs;
    }

    public ArrayList<String> getPredictionCrumbs() {
        return predictionCrumbs;
    }

    @SuppressWarnings("unused")
    public void setPredictionCrumbs(ArrayList<String> predictionCrumbs) {
        this.predictionCrumbs = predictionCrumbs;
    }

    public ArrayList<String> formatCrumbs(){
        return StringTool.extractFormattedPreCrumbs(this);
    }
}
