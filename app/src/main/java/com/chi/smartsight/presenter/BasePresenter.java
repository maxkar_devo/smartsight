package com.chi.smartsight.presenter;


import android.content.Context;

public abstract class BasePresenter<T, N> implements IBasePresenter<T, N> {

    private static final String TAG = BasePresenter.class.getSimpleName();

    private Context context;

    public BasePresenter(Context context) {
        this.context = context;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }
}
