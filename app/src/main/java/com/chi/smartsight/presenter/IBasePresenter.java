package com.chi.smartsight.presenter;


public interface IBasePresenter<T, N>{

    void presentData(N n);

    void stopPresenting();

}
