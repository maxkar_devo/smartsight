package com.chi.smartsight.presenter.camera;


import android.content.Context;
import android.media.Image;
import android.util.Log;

import com.chi.smartsight.application.App;
import com.chi.smartsight.interactor.camera.CameraViewInteractor;
import com.chi.smartsight.listener.camera.ICamDataUser;
import com.chi.smartsight.presenter.BasePresenter;


import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;


public class CameraActivityPresenter extends BasePresenter<Image, ICamDataUser> {

    private static final String TAG = CameraActivityPresenter.class.getSimpleName();

    @Inject
    CameraViewInteractor cameraViewInteractor;

    @SuppressWarnings("unused")
    private Disposable disposable;

    public CameraActivityPresenter(Context context) {
        super(context);

        App.getMainComponent().inject(CameraActivityPresenter.this);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void presentData(ICamDataUser iCamDataUser) {
        cameraViewInteractor.extractDao();
        cameraViewInteractor.interact(iCamDataUser, null);
    }

    @Override
    public void stopPresenting() {
        stopObserving();
        cameraViewInteractor.stopInteraction();
    }

    @SuppressWarnings("unused")
    private Consumer<Void> provideOnNextConsumer() {
        return new Consumer<Void>() {
            @Override
            public void accept(@NonNull Void aVoid) throws Exception {
                Log.d(TAG, "accept: ");
            }
        };
    }

    @SuppressWarnings("unused")
    private Consumer<Throwable> provideOnErrorConsumer() {
        return new Consumer<Throwable>() {
            @Override
            public void accept(@NonNull Throwable throwable) throws Exception {
                Log.d(TAG, "accept() called with: throwable = [" + throwable + "]");
            }
        };
    }

    @SuppressWarnings("unused")
    private Action provideOnCompleteConsumer() {
        return new Action() {
            @Override
            public void run() throws Exception {
                Log.d(TAG, "run() called");
            }
        };
    }

    private void stopObserving() {
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }
    }

}
