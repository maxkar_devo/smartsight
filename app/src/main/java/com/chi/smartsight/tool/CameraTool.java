package com.chi.smartsight.tool;


import android.content.Context;
import android.content.pm.PackageManager;

public class CameraTool {

    public static boolean hasCamera(Context context) {
        return context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA);
    }

}
