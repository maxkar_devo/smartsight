package com.chi.smartsight.tool;


import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;

public class CommonTool {

    public static boolean checkOSVersion(){
        return Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M;
    }

    public static boolean checkIsNetworkAvailable(){
        String command = "ping -c 1 google.com";
        try {
            return (Runtime.getRuntime().exec (command).waitFor() == 0);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    @SuppressWarnings("unused")
    public static String getAppVersion(Context context) {

        PackageInfo info = null;
        try {
            info = context.getPackageManager().getPackageInfo(
                    context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return info != null ? info.versionName : "0";
    }

}
