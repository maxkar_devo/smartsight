package com.chi.smartsight.tool;


import android.support.annotation.NonNull;

import com.chi.smartsight.model.vo.Prediction;

import java.util.ArrayList;
import java.util.Arrays;

public class StringTool {

    public static Prediction extractPrediction(@NonNull String input) {

        return new Prediction(new ArrayList<>(Arrays.asList(input.split(";"))));
    }

    public static float extractFps(String fps) {

        return Float.valueOf(fps.split(" ")[0]);
    }

    public static ArrayList<String> extractFormattedPreCrumbs(Prediction prediction) {

        ArrayList<String> formattedPreCrumbs = new ArrayList<>();

        for (int i = 1; i < prediction.getPredictionCrumbs().size(); i++) {
            if (prediction.getPredictionCrumbs().get(i).split(":").length >= 2) {
                formattedPreCrumbs.add(prediction.getPredictionCrumbs().get(i).split(":")[1]);
            }
        }

        return formattedPreCrumbs;
    }


}
