package com.chi.smartsight.tool;


import android.app.Activity;
import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;

import com.chi.smartsight.R;
import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.touchmenotapps.widget.radialmenu.menu.v1.RadialMenuItem;
import com.touchmenotapps.widget.radialmenu.menu.v1.RadialMenuWidget;

import java.util.ArrayList;
import java.util.List;

public class UiTool {

    /**
     * Initialization of bar chart widget for camera fragment
     */
    @SuppressWarnings("deprecation")
    public static void initFramesBarChart(HorizontalBarChart barChart, Resources resources) {

        int primaryColor = resources.getColor(R.color.colorDarkPrimary);
        int primaryBackColor = resources.getColor(R.color.mainGreyDecor);

        barChart.setDrawBarShadow(false);
        barChart.setDrawValueAboveBar(true);
        barChart.getDescription().setEnabled(false);
        barChart.setMaxVisibleValueCount(10);
        barChart.setPinchZoom(false);
        barChart.setDrawGridBackground(true);
        barChart.setBackgroundColor(primaryBackColor);

        chartXAxisInit(barChart, primaryColor);
        chartYAxisInit(barChart, primaryColor);

    }

    /**
     * Initialization of bar chart y axis
     */
    private static void chartYAxisInit(HorizontalBarChart barChart, int primaryColor) {

        YAxis yl = barChart.getAxisLeft();
        yl.setDrawAxisLine(true);
        yl.setDrawGridLines(true);
        yl.setTextColor(primaryColor);
        yl.setGridColor(primaryColor);
        yl.setAxisMinimum(0f);
        yl.setAxisMaximum(10f);
    }

    /**
     * Initialization of bar chart x axis
     */
    private static void chartXAxisInit(HorizontalBarChart barChart, int primaryColor) {

        XAxis xl = barChart.getXAxis();
        xl.setPosition(XAxis.XAxisPosition.BOTTOM);
        xl.setTextColor(primaryColor);
        xl.setGridColor(primaryColor);
        xl.setDrawAxisLine(true);
        xl.setDrawGridLines(true);
        xl.setAxisMaximum(10f);
    }

    /**
     * Initialization of bar chart widget data for camera fragment
     */
    @SuppressWarnings("deprecation")
    public static void updateChartData(HorizontalBarChart barChart, Resources resources, float value) {

        int primaryColor = resources.getColor(R.color.colorDarkPrimary);

        List<BarEntry> internalEntry = new ArrayList<>();
        internalEntry.add(new BarEntry(0f, value));
        //noinspection SuspiciousNameCombination
        BarDataSet set1 = new BarDataSet(internalEntry, "Frames per Second");
        set1.setValueTextColor(primaryColor);
        set1.setBarBorderColor(primaryColor);
        set1.setDrawIcons(false);
        set1.setColor(primaryColor);
        set1.setHighLightColor(primaryColor);

        ArrayList<IBarDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1);

        BarData data = new BarData(dataSets);
        data.setValueTextSize(10f);
        data.setValueTextColor(primaryColor);
        data.setBarWidth(5f);
        barChart.invalidate();
        barChart.setData(data);
        barChart.notifyDataSetChanged();

    }

    /**
     * Initialization of radial widget menu for camera fragment
     */
    @SuppressWarnings({"deprecation", "ParameterCanBeLocal"})
    public static RadialMenuWidget initCameraPieMenu(RadialMenuWidget pieMenu, Activity activity) {

        int primaryColor = activity.getResources().getColor(R.color.colorDarkPrimary);
        int primaryBackColor = activity.getResources().getColor(R.color.mainGreyDecor);

        pieMenu = new RadialMenuWidget(activity);
        pieMenu.setFocusable(false);

        RadialMenuItem menuCloseItem = new RadialMenuItem("SS", "SS");
        menuCloseItem
                .setDisplayIcon(R.mipmap.ic_smart_eye);
        menuCloseItem
                .setOnMenuItemPressed(new RadialMenuItem.RadialMenuItemClickListener() {
                    @Override
                    public void execute() {
                    }
                });

        final RadialMenuItem pieVisionItem = new RadialMenuItem("Vision", "Vision");
        pieVisionItem
                .setDisplayIcon(R.mipmap.ic_pie_mode);
        pieVisionItem
                .setOnMenuItemPressed(new RadialMenuItem.RadialMenuItemClickListener() {
                    @Override
                    public void execute() {

                    }
                });

        final RadialMenuItem pieActionItem = new RadialMenuItem("Action", "Action");
        pieActionItem
                .setDisplayIcon(R.mipmap.ic_pie_act);
        pieActionItem
                .setOnMenuItemPressed(new RadialMenuItem.RadialMenuItemClickListener() {
                    @Override
                    public void execute() {

                    }
                });

        final RadialMenuItem pieActionItem1 = new RadialMenuItem("Motion", "Motion");
        pieActionItem1
                .setDisplayIcon(R.mipmap.ic_pie_act);
        pieActionItem1
                .setOnMenuItemPressed(new RadialMenuItem.RadialMenuItemClickListener() {
                    @Override
                    public void execute() {

                    }
                });

        final RadialMenuItem pieActionItem2 = new RadialMenuItem("Crop", "Crop");
        pieActionItem2
                .setDisplayIcon(R.mipmap.ic_pie_act);
        pieActionItem2
                .setOnMenuItemPressed(new RadialMenuItem.RadialMenuItemClickListener() {
                    @Override
                    public void execute() {

                    }
                });

        final RadialMenuItem pieActionItem3 = new RadialMenuItem("Text", "Text");
        pieActionItem3
                .setDisplayIcon(R.mipmap.ic_pie_act);
        pieActionItem3
                .setOnMenuItemPressed(new RadialMenuItem.RadialMenuItemClickListener() {
                    @Override
                    public void execute() {

                    }
                });

        List<RadialMenuItem> menuItems = new ArrayList<>();
        menuItems.add(pieActionItem1);
        menuItems.add(pieActionItem2);
        menuItems.add(pieActionItem3);

        pieVisionItem.setMenuChildren(menuItems);

        pieMenu.setAnimationSpeed(100L);
        pieMenu.setSourceLocation(200, 200);
        pieMenu.setIconSize(30, 30);
        pieMenu.setTextSize(13);
        pieMenu.setTextColor(primaryColor, 225);
        pieMenu.setOutlineColor(primaryColor, 225);
        pieMenu.setInnerRingColor(primaryBackColor, 225);
        pieMenu.setOuterRingColor(primaryBackColor, 225);
        pieMenu.setCenterCircle(menuCloseItem);


        pieMenu.addMenuEntry(new ArrayList<RadialMenuItem>() {
            {
                add(pieVisionItem);
                add(pieActionItem);
            }
        });

        return pieMenu;
    }


    /**
     * Runtime view behavior handler
     */
    @SuppressWarnings("unused")
    public static void runtimeViewFormat(Activity activity, View view, boolean flag, int dimens) {

        if (flag) {
            ViewGroup.LayoutParams params = view.getLayoutParams();
            params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
            view.setLayoutParams(params);
        } else {
            view.setMinimumHeight((int) (dimens * activity.getResources().getDisplayMetrics().density + 0.5f));
        }
    }

}
