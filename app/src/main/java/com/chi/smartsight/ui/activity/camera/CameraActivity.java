package com.chi.smartsight.ui.activity.camera;

import android.Manifest;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.pm.PackageManager;
import android.media.Image;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;

import com.chi.smartsight.R;
import com.chi.smartsight.application.App;
import com.chi.smartsight.listener.camera.ICamDataProvider;
import com.chi.smartsight.listener.camera.ICamDataUser;
import com.chi.smartsight.presenter.camera.CameraActivityPresenter;
import com.chi.smartsight.ui.activity.BaseActivity;
import com.chi.smartsight.ui.fragment.camera.CameraFragment;

import javax.inject.Inject;


public class CameraActivity extends BaseActivity implements ICamDataProvider {

    private static final String TAG = CameraActivity.class.getCanonicalName();

    private static final int REQUEST_CAMERA_PERMISSION_RESULT = 999;

    private CameraFragment cameraFragment;

    @Inject
    CameraActivityPresenter activityPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        setTheme(R.style.AppTheme);

        super.onCreate(savedInstanceState);
        App.getMainComponent().inject(this);

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED) {

            initComponents();
            setCurrentFragment(0);
        } else {
            checkPermissions();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case REQUEST_CAMERA_PERMISSION_RESULT: {
                if (grantResults.length > 0
                        || grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    initComponents();
                    setCurrentFragment(0);
                }
            }
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        View decorView = getWindow().getDecorView();
        if (hasFocus) {
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

    @Override
    protected int getContentView() {
        return R.layout.activity_camera;
    }

    @Override
    public void stopListening() {
        activityPresenter.stopPresenting();
    }

    @Override
    public void clearData() {
        Log.d(TAG, "Data cleared");
    }

    @Override
    public Image dataProviding(Image image, ICamDataUser iCamDataUser) {

        activityPresenter.presentData(iCamDataUser);
        return null;
    }

    @Override
    protected void initComponents() {

        cameraFragment = new CameraFragment();
    }

    private void setCurrentFragment(int fragmentNumb) {

        switch (fragmentNumb) {
            case 0:
                replaceFragment(cameraFragment);
                break;
            default:
                replaceFragment(cameraFragment);
                break;
        }
    }

    private void replaceFragment(Fragment fragment) {

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.mainViewHolder, fragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.commit();

    }

    private void checkPermissions() {

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CAMERA},
                    REQUEST_CAMERA_PERMISSION_RESULT);
        }
    }
}

