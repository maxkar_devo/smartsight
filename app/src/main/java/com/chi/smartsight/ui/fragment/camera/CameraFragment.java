package com.chi.smartsight.ui.fragment.camera;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.media.Image;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.chi.smartsight.R;
import com.chi.smartsight.listener.IDataProvider;
import com.chi.smartsight.listener.camera.ICamDataUser;
import com.chi.smartsight.model.vo.Prediction;
import com.chi.smartsight.tool.CommonTool;
import com.chi.smartsight.tool.StringTool;
import com.chi.smartsight.ui.fragment.BaseFragment;
import com.chi.smartsight.ui.fragment.camera.nested.FirstApiFragment;
import com.chi.smartsight.ui.fragment.camera.nested.SecondApiFragment;
import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.touchmenotapps.widget.radialmenu.menu.v1.RadialMenuWidget;

import butterknife.BindView;
import butterknife.OnClick;

import static com.chi.smartsight.tool.UiTool.*;

public class CameraFragment extends BaseFragment implements ICamDataUser{

    private static final String TAG = CameraFragment.class.getCanonicalName();

    @BindView(R.id.surfaceViewHolder)
    FrameLayout frameLayout;

    @BindView(R.id.iv_smart_eye)
    ImageView ivSmartEye;

    @BindView(R.id.predictor_message_one)
    TextView tvPrediction;

    @BindView(R.id.c_bar_chart)
    HorizontalBarChart barChart;

    //widgets
    @SuppressWarnings("unused")
    private RadialMenuWidget pieMenu;

    //fragments
    private FirstApiFragment firstApiFragment;
    private SecondApiFragment secondApiFragment;

    //listeners
    private IDataProvider dataProvider;
    private ICamDataUser childDataUser;

    //markers & variables
    private boolean isPredictionDetailed = false;
    private Prediction prediction;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initComponents();
    }

    @Override
    public void onResume() {
        super.onResume();

        //noinspection unchecked
        dataProvider.dataProviding(null, this);
    }

    @Override
    public void onPause() {
        super.onPause();

        dataProvider.stopListening();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        dataProvider.clearData();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_camera;
    }

    @Override
    public void stopListening() {
        Log.d(TAG, "stopListening");
    }

    @Override
    public void clearData() {
        Log.d(TAG, "Data cleared");
    }

    @Override
    public Image dataObtaining(Image image, String s) {
        return null;
    }

    @Override
    public View provideCamView() {
        return childDataUser.provideCamView();
    }

    @Override
    public void getResult(final String s) {

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                prediction = StringTool.extractPrediction(s);
                updateChartData(barChart, resources,
                        StringTool.extractFps(prediction.getPredictionCrumbs().get(0)));
                predictionTvHandler(prediction);
            }
        });

    }

    @Override
    protected void initComponents() {

        //nested fragments instances creation
        firstApiFragment = new FirstApiFragment();
        secondApiFragment = new SecondApiFragment();

        //listeners & fragments initialization
        childDataUser = (ICamDataUser) setCurrentFragment(CommonTool.checkOSVersion() ? 1 : 0);
        this.dataProvider = (IDataProvider) activity;

        //widgets initialization
        pieMenu = initCameraPieMenu(pieMenu, activity);
        initFramesBarChart(barChart, resources);
        updateChartData(barChart, resources, 3f);
    }

    @OnClick(R.id.iv_smart_eye)
    void smartEyeBtnClick() {

        if (isPredictionDetailed) {
            isPredictionDetailed = false;
            partPredictionText(prediction);
        }

        pieMenu.show(activity.getWindow().getDecorView().findViewById(android.R.id.content),
                0,
                150);
    }

    @OnClick(R.id.predictor_message_one)
    void tvPredictorClick() {

        if (isPredictionDetailed) {
            tvPrediction.setText(partPredictionText(prediction));
        } else {
            tvPrediction.setText(fullPredictionText(prediction));
        }
    }

    /**
     * Current fragment setter
     */
    private Fragment setCurrentFragment(int fragmentNumb) {

        switch (fragmentNumb) {
            case 0:
                replaceFragment(firstApiFragment);
                return firstApiFragment;
            case 1:
                replaceFragment(secondApiFragment);
                return secondApiFragment;
            default:
                replaceFragment(firstApiFragment);
                return firstApiFragment;
        }
    }

    /**
     * Fragment replacement handler method
     */
    private Fragment replaceFragment(Fragment fragment) {

        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        ft.replace(R.id.surfaceViewHolder, fragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.commit();

        return fragment;

    }

    /**
     * Prediction content internal handler
     */
    private String fullPredictionText(Prediction p) {

        isPredictionDetailed = true;
        StringBuilder builder = new StringBuilder("\n");

        for (String crumb : p.formatCrumbs()) {
            builder.append(crumb).append("\n");
        }

        return builder.toString();
    }

    /**
     * Prediction content internal handler
     */
    private String partPredictionText(Prediction p) {

        isPredictionDetailed = false;
        return p.formatCrumbs().get(0) + "...";
    }

    /**
     * Nested view behavior handler
     */
    private void predictionTvHandler(Prediction p) {

        if (!isPredictionDetailed) {
            tvPrediction.setText(partPredictionText(p));
        } else {
            tvPrediction.setText(fullPredictionText(p));
        }
    }
}
