package com.chi.smartsight.ui.fragment.camera.nested;

import android.media.Image;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.TextureView;
import android.view.View;

import com.chi.smartsight.R;
import com.chi.smartsight.listener.camera.ICamDataUser;
import com.chi.smartsight.ui.fragment.BaseFragment;

import butterknife.BindView;


public class SecondApiFragment extends BaseFragment implements ICamDataUser {

    private static final String TAG = SecondApiFragment.class.getCanonicalName();

    @BindView(R.id.textureView)
    TextureView textureView;

    @SuppressWarnings("unused")
    private ICamDataUser parentDataUser;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initComponents();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_camera_api_two;
    }

    @Override
    public void stopListening() {
        Log.d(TAG, "stopListening: ");
    }

    @Override
    public void clearData() {
        Log.d(TAG, "clearData: ");
    }

    @Override
    public Image dataObtaining(Image image, String s) {
        Log.d(TAG, "dataObtaining: ");
        return null;
    }

    @Override
    public TextureView provideCamView() {
        return textureView;
    }

    @Override
    public void getResult(String s) {
        Log.d(TAG, "getResult: ");
    }

    @Override
    protected void initComponents() {
        parentDataUser = (ICamDataUser) getParentFragment();
    }
}
